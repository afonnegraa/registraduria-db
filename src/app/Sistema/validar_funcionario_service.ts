import * as axios from 'axios';

export default class ValidarFuncionarioService  {
    /*
    CONSULTA DE INFORMACION DEL FUNCIONARIO 
    DE FUERZA PUBLICA EN LA BASE DE DATOS 
    DEL DEPARTAMENTO DE DEFENSA
    */
    validateRegistration(identificacion: any): any {
            return axios.default
            .post('https://us-central1-departamento-de-defensa.cloudfunctions.net/getFuncionarioService',identificacion)
            .then((res: any) => {
                return res.data; //RETORNA DATOS DE LA BASE DE DATOS DEL DEPARTAMENTO DE DEFENSA
            })
            .catch((error: any) => {
                return error;
            });
    }
}