import * as functions from 'firebase-functions';
import * as sgMail from '@sendgrid/mail'

export default class confirmacionCorreoService {
    public sendEmail(data: any) {
        const API_KEY = functions.config().sengrid.key;
        sgMail.setApiKey(API_KEY);
        //FORMATO DEL CORREO
        const email = {
            from: 'Registraduria El Bosque <no-reply@regisubosque.com>',
            to: data.email,
            subject: 'Registro exitoso',
            html: `
            <h1>El registro ha sido exitoso</h1>
            <p style="font-size: 16px;">Debe tener en cuenta que para ingresar:</p>
            <ul>
            <li><b>Su cuenta de usuario es:</b> ${data.identificacion}</li>
            <li><b>La contraseña es:</b> ${data.password}</li>
            </ul>
            `
        };

        // ENVIAR CORREO Y RETORNAR RESULTADO
        return sgMail.send(email).then(() => {
            return 'EMAIL SEND';
        }).catch((err: any) => {
            return err;
        });

    };

}
