import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import ValidarFuncionarioService from '../Sistema/validar_funcionario_service';
import confirmacionCorreoService from '../Sistema/confirmacion_correo_service';


export const registroService = functions.https.onRequest(async (req, res) => {
    const data = req.body.data;
    //VERIFICACIÓN DE EXISTENCIA EN LA FUERZA PÚBLICA
    let existe = await admin.firestore().collection('funcionario').doc(data.identificacion).get().then((response: any) => {
        //SI EL NO FUNCIONARIO EXISTE EN LA REGISTRADURIA
        if (!response.exists) {
            //CONSULTAR LA BASE DE DATOS DEL DEPARTAMENTO DE DEFENSA
           return 'NO_DATA';
        } else {
            return 'USUARIO_REGISTRADO';
        }
    });

    if (existe === 'NO_DATA') {
        const dataFuncionarioReq = {
            identificacion: data.identificacion
        }
        const validarFuncionario = new ValidarFuncionarioService();
        const dataFuncionarioRes = await validarFuncionario.validateRegistration(dataFuncionarioReq);
        //SI EL USUARIO EXISTE EN LA BASE DE DATOS DEL DEPARTAMENTO DE DEFENSA
        if (dataFuncionarioRes !== null) {
            //VALIDAR QUE LOS DATOS DEL REGISTRO SEAN LOS MISMOS DATOS DE LA BASE DE DATOS DEL DEPARTAMENTO DE DEFENSA
            if(
                dataFuncionarioRes.idFuerza === data.idFuerza && 
                dataFuncionarioRes.idRango === data.idRango && 
                dataFuncionarioRes.idFuncionario.toString() === data.idFuncionario.toString() &&
                dataFuncionarioRes.identificacion.toString() === data.identificacion.toString()
            ) {
                existe = 'EXISTE';
            } else {
                existe = 'EXISTE_SIN_RELACION';
            }
        } else {
            existe = 'NO_EXISTE';
        }
    }
    //SI EXISTE EN LA BASE DE DATOS DEL DEPTO DE DEFENSA
    if(existe === 'EXISTE') {
        //CREACION DEL USUARIO
        const generatedPassword = passwordGen();
        let userData: any;

        //GUARDAR LA INFORMACION DEL FUNCIONARIO EN LA BASE DE DATOS
        await admin.firestore().collection('funcionario').doc(data.identificacion.toString()).set({
            idFuerza: data.idFuerza,
            idRango: data.idRango,
            identificacion: data.identificacion,
            nombres: data.nombres,
            apellidos: data.apellidos,
            correo: data.correo,
            idFuncionario: data.idFuncionario
        });

        //CREAR EL USUARIO Y CREDENCIALES
        await admin.auth().createUser({
            uid: data.identificacion.toString(),
            email: data.correo,
            password: generatedPassword,
            emailVerified: true,
            displayName: data.nombres,
            disabled: false
        }).then((user) => {
            userData = user;
        }).catch((err) => {
            return 'NO_SE_CREO';
        });

        //CREACION DE ROLES
        await admin.auth().setCustomUserClaims(userData.uid, {
            administrador: false,
            fuerzaPublica: true
        }).catch((err) => {
            throw new functions.https.HttpsError('invalid-argument', 'Error creando CC');
        });

        //ENVIAR CORREO AL USUARIO CREADO
        const dataMail = {
            email: data.correo,
            identificacion: data.identificacion.toString(),
            password: generatedPassword
        }
        const email = new confirmacionCorreoService();
        await email.sendEmail(dataMail);
        res.send({data: 'REGISTRO_EXITOSO'});
    } else {
        res.send({data: existe});
    }
    
   
 

});

//GENERADOR DE CONTRASEÑAS
function passwordGen(): string {
    let pass = '';
    const abc = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

    for (let i = 0; i< 6; i++) {
        const randomLetter = Math.floor(Math.random() * 25);
        const randomMayus = Math.floor(Math.random() * 3);
        let letter = abc[randomLetter];
        if(randomMayus === 0) {
            letter = letter.toLowerCase(); // LETRA MINUSCULA
        } else if (randomMayus === 1) {
            letter = (Math.floor(Math.random() * 10) -1).toString(); //NUMERO
        }
        pass = pass + letter;
    }
    return pass;
}
