import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

interface reqdata {
    'identificacion': string
}

export const lecturaDocumentoService = functions.https.onCall(async (data: reqdata, context:any) => {
    // REVISAR SI EL REQUEST SE HIZO DE UN USUARIO ACTIVO
    if (context) {
        if (context.auth.token.fuerzaPublica !== true) {
           return 'NO_PERMITIDO';
        }
    } else {
        return 'NO_PERMITIDO';
    }
    //RETORNAR DATOS DE IDENTIFICACION
    const response = await admin.firestore().collection('ciudadanos').doc(data.identificacion).get().then(
        (res) => {
            if (!res.exists) {
                return 'NO_EXISTE';
            }
            const result: any = res.data();
            return result;
        }
    ).catch(() => {
        return 'ERROR';
    });
   
    return response;
});