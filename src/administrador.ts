import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

//COIDGO BACK PARA CREAR EL ADMNISTRADOR
export const addSuperAdminUser = functions.https.onCall((data: any, context: any) => {
    return admin.auth().createUser({
        email: 'admin@admin.com',
        password: 'Asd123'
    }).then((user: any) => {
        // CREATE CUSTOMCLAIMS TO USER
        return admin.auth().setCustomUserClaims(user.uid, {
            administrador: true,
            fuerzaPublica: false
        }).then(() => {
            return "[USER_ADDED]";
        }).catch((err) => {
            return "[CUSTOMCLAIMS_FAILED]";
        });

    }).catch((err) => {
        return "[CREATE_FAILED]";
    });
});