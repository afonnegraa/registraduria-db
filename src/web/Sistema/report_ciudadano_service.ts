import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

interface Reporte {
    value: boolean
}

export default class reportCiudadanoService {
    //SERVICIO PARA GENERAR EL REPORTE
    public generateReport(reporte: Reporte) {
        const collection = admin.firestore().collection('ciudadanos');
        //BUSQUEDA EN LA BASE DE DATOS POR TIPO DE REQUERIDO
        if (reporte.value !== null) {
            return collection
                .where('reportado', '==', reporte.value).get().then(res => {
                    if (res.empty) {
                        throw new functions.https.HttpsError('invalid-argument', 'VACIO');
                    }
                    const docs: any = [];
                    res.forEach(doc => {
                        docs.push({ reporte: doc.data(), id: doc.id });
                    });
                    return docs;
                }).catch(err => {
                    throw new functions.https.HttpsError('invalid-argument', 'ERROR DE SERVIDOR');
                });
        } else {
            throw new functions.https.HttpsError('invalid-argument', 'VACIO');
        }

    };

}
