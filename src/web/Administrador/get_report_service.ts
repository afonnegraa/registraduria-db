import * as functions from 'firebase-functions';
import reportCiudadanoService from '../Sistema/report_ciudadano_service';

interface Reporte {
    value: boolean
}

// SERVICIO DE REPORTES ADMINISTRADOR
export const getReportService = functions.https.onCall( async (reporte: Reporte, context: any) => {
    // REVISAR SI EL REQUEST SE HIZO DE UN USUARIO ACTIVO
    if (context) {
        if (context.auth.token.administrador !== true) {
            throw new functions.https.HttpsError('invalid-argument', 'NO PERMITIDO');
        }
    } else {
        throw new functions.https.HttpsError('invalid-argument', 'NO PERMITIDO');
    }

     //GENERAR REPORTE DEL SISTEMA
     
    const reportCiudadano = new reportCiudadanoService();
    //SE CONSULTA AL SISTEMA PARA QUE GENERE EL REPORTE
    const result = await reportCiudadano.generateReport(reporte);

    return result;
    
});

