import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

interface ciudadanoData {
    rH: string,
    apellidos: string,
    estatura: string,
    fechaExpedicion: string,
    fechaNacimiento: string,
    grupoSanguineo: string,
    identificacion: string,
    lugarExpedicion: string,
    lugarNacimiento: string,
    nombres: string,
    reportado: string
};

// EDITAR CIUDADANO SERVICE
export const editCiudadanoService = functions.https.onCall(async (data: ciudadanoData, context: any) => {

    // REVISAR SI EL REQUEST SE HIZO DE UN ADMINISTRADOR ACTIVO
    if (context) {
        if (context.auth.token.administrador !== true) {
            throw new functions.https.HttpsError('invalid-argument', 'NO PERMITIDO');
        }
    } else {
        throw new functions.https.HttpsError('invalid-argument', 'NO PERMITIDO');
    }

    // EDITAR CIUDADANO
    await admin.firestore().collection('ciudadanos').doc(data.identificacion.toString()).update({
        identificacion: data.identificacion,
        nombres: data.nombres,
        apellidos: data.apellidos,
        fechaNacimiento: data.fechaNacimiento,
        lugarNacimiento: data.lugarNacimiento,
        fechaExpedicion: data.fechaExpedicion,
        lugarExpedicion: data.lugarExpedicion,
        rH: data.rH,
        grupoSanguineo: data.grupoSanguineo,
        estatura: data.estatura,
        reportado: data.reportado
    }).catch((err) => {
        throw new functions.https.HttpsError('invalid-argument', 'ERROR NO SE PUDO EDITAR EL CIUDADANO');
    });

    return 'EDICION_EXITOSA'

});

