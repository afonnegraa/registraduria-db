import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

interface filters {
    value: string
}

// LISTA DE CIUDADANOS SERVICIO
export const listCiudadanoService = functions.https.onCall((data: filters, context: any) => {
    // REVISAR SI EL REQUEST SE HIZO DE UN USUARIO ACTIVO
    if (context) {
        if (context.auth.token.administrador !== true) {
            throw new functions.https.HttpsError('invalid-argument', 'NO PERMITIDO');
        }
    } else {
        throw new functions.https.HttpsError('invalid-argument', 'NO PERMITIDO');
    }

    //BUSCAR CIUDADANO POR DOCUMENTO DE IDENTIFICACION
    const collection = admin.firestore().collection('ciudadanos');
    if(data.value.length>0) {
        return collection
        .where('identificacion', '==', data.value).get().then(res => {
            if (res.empty) {
                throw new functions.https.HttpsError('invalid-argument', 'VACIO');
            }
            const docs: any = [];
            res.forEach(doc => {
                docs.push({ data: doc.data(), id: doc.id });
            });
            return docs;
        }).catch(err => {
            throw new functions.https.HttpsError('invalid-argument', 'ERROR DE SERVIDOR');
        });
    } else {
        //TRAER EL LISTADO DE TODOS LOS CIUDADANOS
        return collection.get().then(res => {
            if (res.empty) {
                throw new functions.https.HttpsError('invalid-argument', 'VACIO');
            }
            const docs: any = [];
            res.forEach(doc => {
                docs.push({ data: doc.data(), id: doc.id });
            });
            return docs;
        }).catch(err => {
            throw new functions.https.HttpsError('invalid-argument', 'ERROR DE SERVIDOR');
        });
    }
});

