import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

interface ciudadanoData {
    rH: string,
    apellidos: string,
    estatura: number,
    fechaExpedicion: string,
    fechaNacimiento: string,
    grupoSanguineo: string,
    identificacion: number,
    lugarExpedicion: string,
    lugarNacimiento: string,
    nombres: string,
    reportado: boolean
};


// CREAR CIUDADANO SERVICIO
export const createCiudadanoService = functions.https.onCall(async (data: ciudadanoData, context: any) => {

    // REVISAR SI EL REQUEST SE HIZO DE UN ADMINISTRADOR ACTIVO
    if (context) {
        if (context.auth.token.administrador !== true) {
            throw new functions.https.HttpsError('invalid-argument', 'NO PERMITIDO');
        }
    } else {
        throw new functions.https.HttpsError('invalid-argument', 'NO PERMITIDO');
    }

    //VALIDACION DE EXISTENCIA DE CIUDADANO
    const existe = await admin.firestore().collection('ciudadanos').doc(data.identificacion.toString()).get().then(
        (res) => {
            if (!res.exists) {
                return false;
            } else {
                return true;
            }
        }
    );

    if (existe) {
        throw new functions.https.HttpsError('invalid-argument', 'ERROR EL CIUDADANO YA SE ENCUENTRA REGISTRADO');
    } else {

        // CREACIÓN DE CIUDADANO
        await admin.firestore().collection('ciudadanos').doc(data.identificacion.toString()).set({
            identificacion: data.identificacion,
            nombres: data.nombres,
            apellidos: data.apellidos,
            fechaNacimiento: data.fechaNacimiento,
            lugarNacimiento: data.lugarNacimiento,
            fechaExpedicion: data.fechaExpedicion,
            lugarExpedicion: data.lugarExpedicion,
            rH: data.rH,
            grupoSanguineo: data.grupoSanguineo,
            estatura: data.estatura,
            reportado: data.reportado
        }).catch((err) => {
            throw new functions.https.HttpsError('invalid-argument', 'ERROR NO SE PUDO CREAR EL CIUDADANO');
        });

        return 'CREACION_EXITOSA';
    }

});


