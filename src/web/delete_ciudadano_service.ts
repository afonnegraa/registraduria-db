import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

interface ciudadanoData {
    identificacion: string
};

// ELIMINAR CIUDADANO SERVICIO
export const deleteCiudadanoService = functions.https.onCall(async (data: ciudadanoData, context: any) => {

    // REVISAR SI EL REQUEST SE HIZO DE UN USUARIO ACTIVO
    if (context) {
        if (context.auth.token.administrador !== true) {
            throw new functions.https.HttpsError('invalid-argument', 'NO PERMITIDO');
        }
    } else {
        throw new functions.https.HttpsError('invalid-argument', 'NO PERMITIDO');
    }

    // ELIMINAR CIUDADANO
    await admin.firestore().collection('ciudadanos').doc(data.identificacion.toString()).delete().catch((err) => {
        throw new functions.https.HttpsError('invalid-argument', 'ERROR NO SE PUDO ELIMINAR EL CIUDADANO');
    });

    return 'ELIMINACION_EXITOSA'

});

