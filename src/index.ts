import * as admin from 'firebase-admin';

//IMPORT APLICACION
import { registroService } from './app/Funcionario/registro_service';
import { lecturaDocumentoService } from './app/Funcionario/lectura_documento_service';
//EXPORT APLICACION
export { registroService, lecturaDocumentoService }

//IMPORT WEB
import { listCiudadanoService } from './web/Administrador/list_ciudadano_service';
import { createCiudadanoService } from './web/Administrador/create_ciudadano_service';
import { editCiudadanoService } from './web/Administrador/edit_ciudadano_service';
import { deleteCiudadanoService } from './web/delete_ciudadano_service';
import { getReportService } from './web/Administrador/get_report_service';
import { addSuperAdminUser } from './administrador';

//EXPORT WEB
export { listCiudadanoService, createCiudadanoService, editCiudadanoService, deleteCiudadanoService, getReportService, addSuperAdminUser }

admin.initializeApp();